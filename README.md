# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kanaujia/lara-multipurpose.svg?style=flat-square)](https://packagist.org/packages/kanaujia/lara-multipurpose)
[![Build Status](https://img.shields.io/travis/kanaujia/lara-multipurpose/master.svg?style=flat-square)](https://travis-ci.org/kanaujia/lara-multipurpose)
[![Quality Score](https://img.shields.io/scrutinizer/g/kanaujia/lara-multipurpose.svg?style=flat-square)](https://scrutinizer-ci.com/g/kanaujia/lara-multipurpose)
[![Total Downloads](https://img.shields.io/packagist/dt/kanaujia/lara-multipurpose.svg?style=flat-square)](https://packagist.org/packages/kanaujia/lara-multipurpose)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require kanaujia/lara-multipurpose
```

## Usage

``` php
// Usage description here
```

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email vijaykanaujia3@gmail.com instead of using the issue tracker.

## Credits

- [vijay kanaujia](https://github.com/kanaujia)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Laravel Package Boilerplate

This package was generated using the [Laravel Package Boilerplate](https://laravelpackageboilerplate.com).