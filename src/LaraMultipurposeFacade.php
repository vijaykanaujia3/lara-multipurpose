<?php

namespace Kanaujia\LaraMultipurpose;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Kanaujia\LaraMultipurpose\Skeleton\SkeletonClass
 */
class LaraMultipurposeFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'lara-multipurpose';
    }
}
